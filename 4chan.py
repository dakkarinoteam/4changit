﻿#!/usr/bin/python3.4
# -*- coding: utf-8 -*-
'''µv3'''
#C:\lavoro\#python_laptop\4chan
''''''
#from httplib import BadStatusLine
from pathlib import Path
import datetime
import hashlib
import http.client
import json
import os
import string
import time
import unicodedata
import urllib.request
import urllib.error
import socket
''''''
''''''
socket.setdefaulttimeout(15)
''''''
def debug(what):
	printing = False
	verbose = True
	debug_file = '4chan_debug.txt'
	timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
	what = timestamp+'\t'+what+'\r'
	if printing:
		print(what)
	if verbose:
		with open(debug_file, 'a', encoding='utf-8-sig') as file:
			file.write(what)
''''''
def check_filename(filename, what):
	#debug('BOF: check_filename()')
	if not os.path.isfile(filename):
		return False
	with open(filename, encoding='utf-8-sig') as file:
		for line in file:
			if what in line:
				#debug('EOF: check_filename() #True')
				return True
	#debug('EOF: check_filename() #False')
	return False
''''''
def write_file(filename, what):
	debug('BOF: write_file()')
	with open(filename, 'a', encoding='utf-8-sig') as file:
		file.write(what+'\n')
	debug('EOF: write_file()')
''''''
def removing(path):
	if os.path.isfile(path):
		debug('Removing incomplete file: {}'.format(path))
		print('Removing incomplete file: {}'.format(path))
		os.remove(path)
		debug('Removed incomplete file: {}'.format(path))
		print('Removed incomplete file: {}'.format(path))
''''''
def save_img(url, path, filename):
	timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
	debug('BOF: save_img()')
	debug('{} -> downloading...'.format(filename))
	print('{} {} -> downloading...'.format(timestamp, filename))
	try:
		urllib.request.urlretrieve(url, path)
	except (urllib.error.HTTPError) as e:
		debug('***001*** Error: {}.'.format(e))
		print('{} ***001*** Error: {}.'.format(timestamp, e))
	except (urllib.error.URLError) as e:
		debug('***001*** Error: {}.'.format(e))
		print('{} ***001*** Error: {}.'.format(timestamp, e))
		removing(path)
	except ConnectionResetError as pd:
		debug('***002*** Error: {}.'.format(pd))
		print('{} ***002*** Error: {}.'.format(timestamp, pd))
	except ConnectionError as mp:
		debug('***003*** Error: {}.'.format(mp))
		print('{} ***003*** Error: {}.'.format(timestamp, mp))
	except (http.client.IncompleteRead, http.client.BadStatusLine) as fu:
		debug('***004*** Error: {}.'.format(fu))
		print('{} ***004*** Error: {}.'.format(timestamp, fu))
	except (urllib.error.ContentTooShortError) as e:
		debug('***005*** Error: {}.'.format(e))
		print('{} ***005*** Error: {}.'.format(timestamp, e))
		removing(path)
	except IOError as e:
		debug('***006*** IOError: {} ({}, {}, {}).'.format(e, url, path, filename))
		print('{} ***006*** IOError: {} ({}, {}, {}).'.format(timestamp, e, url, path, filename))
		removing(path)
		#time.sleep(10)
	else:
		debug('{} -> downloaded.'.format(filename))
		debug('EOF: save_img()')
''''''
def save_thread(thread):
	timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
	debug('BOF: save_thread()')
	debug('Downloading thread #{}...'.format(thread))
	#base_url_json = 'https://a.4cdn.org/b/thread/'
	#posts = json_loads(base_url_json+thread+'.json')
	url_thread = 'https://a.4cdn.org/b/thread/{}.json'.format(thread)
	thread_json = '{}_{:%Y%m%d%H%M%S}.json'.format(thread, datetime.datetime.now())
	#thread_json = '{}.json'.format(thread)
	path = 'json_{}/'.format(thread)
	path_thread = Path(path)
	if not path_thread.is_dir():
		path_thread.mkdir()
	try:
		binary = urllib.request.urlopen(url_thread).read()
	except (urllib.error.HTTPError, urllib.error.URLError, urllib.error.ContentTooShortError) as e:
		debug('>>>001>>> Error: {}.'.format(e))
		print('{} >>>001>>> Error: {}.'.format(timestamp, e))
	except (http.client.IncompleteRead, http.client.BadStatusLine) as e:
		debug('>>>002>>> Error: {}.'.format(e))
		print('{} >>>002>>> Error: {}.'.format(timestamp, e))
	except IOError as fu:
		debug('>>>003>>> IOError: {}.'.format(fu))
		print('{} >>>003>>> IOError: {}.'.format(timestamp, fu))
	else:
		with open(path+thread_json, 'w', encoding='utf-8-sig') as f:
			debug('>>> Saving thread #{}...'.format(thread))
			f.write(binary.decode('utf-8'))
			debug('>>> Saved thread #{}.'.format(thread))
		debug('Downloaded thread #{}.'.format(thread))
		debug('EOF: save_thread()')
''''''
def json_loads(json_url):
	timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
	debug('BOF: json_loads()')
	try:
		debug('Getting {}...'.format(json_url))
		json_loads = json.loads(urllib.request.urlopen(json_url).read().decode('utf-8'))
		debug('EOF: json_loads()')
	except (urllib.error.HTTPError, urllib.error.URLError, urllib.error.ContentTooShortError) as e:
		debug(':::001::: Error: {}.'.format(e))
		print('{} :::001::: Error: {}.'.format(timestamp, e))
	except (urllib.error.HTTPError, TimeoutError, ConnectionResetError) as e:
		debug(':::002::: Error: {}.'.format(e))
		print('{} :::002::: Error: {}.'.format(timestamp, e))
	except ConnectionResetError as e:
		debug(':::003::: Error: {}.'.format(e))
		print('{} :::003::: Error: {}.'.format(timestamp, e))
	except ConnectionError as e:
		debug(':::004::: Error: {}.'.format(e))
		print('{} :::004::: Error: {}.'.format(timestamp, e))
	except (http.client.IncompleteRead, http.client.BadStatusLine) as e:
		debug(':::005::: Error: {}.'.format(e))
		print('{} :::005::: Error: {}.'.format(timestamp, e))
	except IOError as e:
		debug(':::006::: IOError: {} ({}).'.format(e, json_url))
		print('{} :::006::: IOError: {} ({}).'.format(timestamp, e, json_url))
	except UnicodeDecodeError as e:
		debug(':::007::: Error: {}.'.format(e))
		print('{} :::007::: Error: {}.'.format(timestamp, e))
	except ValueError as e:
		debug(':::008::: Error: {}.'.format(e))
		print('{} :::008::: Error: {}.'.format(timestamp, e))
	else:
		debug('Got {}.'.format(json_url))
		debug('EOF: json_loads()')
		return json_loads
''''''
def get_threads():
	debug('BOF: get_threads()')
	debug('{:%Y-%m-%d %H:%M:%S}: Scanning the catalog. Please wait...'.format(datetime.datetime.now()))
	print('{:%Y-%m-%d %H:%M:%S}: Scanning the catalog. Please wait...'.format(datetime.datetime.now()))
	threads = []
	catalog = 'https://a.4cdn.org/b/catalog.json'
	pages = json_loads(catalog)
	if pages is not None:
		for page in pages:
			for thread in page['threads']:
				try:
					findme = 'draw'
					if ('sub' in thread):
						if findme in thread['sub'].lower():
							print('--- DRAW SUB FOUND: {} in thread {}'.format(thread['sub'],thread['no']))
							threads.append(thread['no'])
					if findme in thread['com'].lower() or ('filename' in thread and findme in thread['filename'].lower()):
						threads.append(thread['no'])
				except KeyError as e:
					debug('---001--- Error: {}.'.format(e))
					print('---001--- Error: {}.'.format(e))
					#print(thread)
				except IndexError as e:
					debug('---002--- Error: {}.'.format(e))
					print('---002--- Error: {}.'.format(e))
	debug('Done.')
	debug('EOF: get_threads()')
	return threads
''''''
def get_imgs(threads):
	debug('BOF: get_imgs()')
	debug('Thread(s) found. Proceeding...')
	debug('{:%Y-%m-%d %H:%M:%S}: Found {} thread(s).'.format(datetime.datetime.now(), len(threads)))
	print('{:%Y-%m-%d %H:%M:%S}: Found the following {} thread(s):'.format(datetime.datetime.now(), len(threads)))
	#debug(threads)
	print(threads)
	debug('Downloading attachments. Please wait...')
	print('Downloading attachments. Please wait...')
	base_url_json = 'https://a.4cdn.org/b/thread/'
	base_url_img = 'https://i.4cdn.org/b/'
	for thread in threads:
		#debug('Found threads in catalog. Proceeding...')
		thread = str(thread)
		path = thread+'/'
		path_thread = Path(thread)
		if not path_thread.is_dir():
			path_thread.mkdir()
		save_thread(thread)
		debug('Getting thread JSON...')
		posts = json_loads(base_url_json+thread+'.json')
		debug('Checking posts for images...')
		if posts:
			debug('Post(s) found in thread.')
			for post in posts['posts']:
				if 'tim' in post:
					#debug('Image found in post.')
					what = str(post['tim'])
					filename = thread + '.txt'
					#debug('Saving filename.')
					if not check_filename(filename, what):
						debug('Saving filename...')
						what = '{}\t{}\t{}{}'.format(thread, post['tim'], post['filename'], post['ext'])
						write_file(filename, what)
						debug('Filename saved in {}.'.format(filename))
					post_time = post['time']
					filename = post['filename'][:80]
					filename = '{}_{}{}'.format(post['tim'],filename,post['ext'])
					url = base_url_img + str(post['tim']) + post['ext']
					#This is to maintain interoperability
					filename = filename.translate({ord(c):'_' for c in '<>:"\|?*'})
					filename = ''.join(ch for ch in filename if unicodedata.category(ch)[0]!='C')
					#print('Filename: {}'.format(filename))
					path_file = path_thread / filename
					path_ = path+filename
					if not path_file.exists():
						debug('Saving image {}...'.format(filename))
						save_img(url, path_, filename)
						debug('Image saved.')
			debug('Finished saving image(s) for thread #{}.'.format(thread))
			print('Finished saving image(s) for thread #{}.'.format(thread))
	#print('I was watching the following threads:')
	#print('{}\n------------------------------'.format(threads))
	debug('EOF: get_imgs()')
	#debug('------------------------------')
''''''
if __name__=='__main__':
	debug('------------------------------')
	print('------------------------------')
	while True:
		debug('Checking threads.')
		threads = get_threads()
		debug('Done.')
		#threads = [607703658, 607709445]
		#threads.append(726200166,)
		#threads.append(727390386)
		#threads.append(894614368)
		interval = 15
		for x in range(int(60/interval)):
			if threads:
				debug('Thread(s) found!')
				debug(str(threads))
				imgs = get_imgs(threads)
				debug('------------------------------')
				print('------------------------------')
			else:
				debug('{:%Y-%m-%d %H:%M:%S}: Nothing to scan!\n------------------------------'.format(datetime.datetime.now()))
				print('{:%Y-%m-%d %H:%M:%S}: Nothing to scan!\n------------------------------'.format(datetime.datetime.now()))
			time.sleep(interval)
''''''