﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
'''µv3'''
#C:\lavoro\#python_laptop\4chan
''''''
import datetime
import http.client
import json
import os
import socket
import time
import urllib.request
import urllib.error
import webbrowser
''''''
socket.setdefaulttimeout(10)
''''''
''''''
''''''
def get_threads_from_file(filename):
	if not os.path.isfile(filename):
		return []
	with open(filename, encoding='utf-8-sig') as file:
		threads=[line.strip() for line in file]
	if threads:
		return threads
	return []
''''''
def json_loads(json_url):
	try:
		json_loads = json.loads(urllib.request.urlopen(json_url).read().decode('utf-8'))
	except (urllib.error.HTTPError, urllib.error.URLError, urllib.error.ContentTooShortError) as e:
		print(' :::001::: Error: {}.'.format(e))
	except (urllib.error.HTTPError, TimeoutError, ConnectionResetError) as e:
		print(' :::002::: Error: {}.'.format(e))
	except ConnectionResetError as e:
		print(' :::003::: Error: {}.'.format(e))
	except ConnectionError as e:
		print(' :::004::: Error: {}.'.format(e))
	except (http.client.IncompleteRead, http.client.BadStatusLine) as e:
		print(' :::005::: Error: {}.'.format(e))
	except IOError as e:
		print(' :::006::: IOError: {}.'.format(e))
	else:
		return json_loads
''''''
def get_threads():
	print('{:%Y-%m-%d %H:%M:%S}: Scanning the catalog. Please wait...'.format(datetime.datetime.now()))
	threads = []
	catalog = 'https://a.4cdn.org/b/catalog.json'
	pages = json_loads(catalog)
	if pages is not None:
		for page in pages:
			for thread in page['threads']:
				#if 'draw' in thread['com'].lower():
				#if 'draw' in thread['com'].lower() or 'draw' in thread['filename'].lower():
				try:
					if 'draw' in thread['com'].lower() or ('filename' in thread and 'draw' in thread['filename'].lower()):
						threads.append(thread['no'])
				except KeyError as e:
					print(' ***007*** Error: {}.'.format(e))
				except IndexError as e:
					print(' ***008*** Error: {}.'.format(e))
	return threads
''''''
def open_thread(thread):
	base_url = 'https://boards.4chan.org/b/thread/'
	webbrowser.open_new_tab('{}{}'.format(base_url,thread))
	print('{:%Y-%m-%d %H:%M:%S}: {}{}'.format(datetime.datetime.now(),base_url,thread))
''''''
def opened(opened_threads_file,thread):
	with open(opened_threads_file, 'a', encoding='utf-8-sig') as file:
		file.write('{}\n'.format(thread))
''''''
if __name__=='__main__':
	interval_big=600
	interval_little=10
	interval=10
	while True:
		opened_threads_file = 'opened_threads_file.txt'
		opened_threads = get_threads_from_file(opened_threads_file)
		threads=get_threads()
		threads=list(set(threads)-set(map(int, opened_threads)))
		for thread in threads:
			open_thread(thread)
			opened(opened_threads_file,thread)
			time.sleep(interval)
		time.sleep(int(interval_big/interval_little))
''''''
